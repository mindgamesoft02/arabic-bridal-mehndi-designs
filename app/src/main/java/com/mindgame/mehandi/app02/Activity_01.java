package com.mindgame.mehandi.app02;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Handler;

public class Activity_01 extends AppCompatActivity  {
    Button btn1,btn2,btn3,btn4;
    TextView txt1,txt2,txt3,txt4;
    ArrayList<String>images_json;
    Context context;
    InterstitialAd interstitialAd;
    AdMobdetails_singleton AdMob = AdMobdetails_singleton.getInstance();
    String interstitial0, interstitial1,
             interstitial2,interstitial3,interstitial4;
    final String INTERSTITIAL_KEY = "interstitial";
    AdRequest adRequest = new AdRequest.Builder().build();
    Handler handler;

    LinearLayout layout, strip, layout1, strip1;
//    ProgressDialog ringProgressDialog=new ProgressDialog(this);
    ProgressDialog pd;
    Context c;
    Boolean ads_variable;
    String TAG=Activity_01.this.getClass().getSimpleName();
    Boolean network;
   java.sql.Timestamp timestamp;
    long current_time;
    TextView Title2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch);
        context=getApplicationContext();
        Title2 = (TextView) findViewById(R.id.textView2);
        btn1=(Button)findViewById(R.id.btn1);
        btn2=(Button)findViewById(R.id.btn2);
        btn3=(Button)findViewById(R.id.btn3);
        btn4=(Button)findViewById(R.id.btn4);
        btn1.setBackgroundResource(R.drawable.set_1);
        btn2.setBackgroundResource(R.drawable.set_2);
        btn3.setBackgroundResource(R.drawable.set_3);
        btn4.setBackgroundResource(R.drawable.set_4);
        interstitialAd=new InterstitialAd(this);

        /*Get Ad Unit ids*/
        interstitial0 = AdMob.get_admob_id(0,INTERSTITIAL_KEY);
        interstitial1 = AdMob.get_admob_id(1,INTERSTITIAL_KEY);
        interstitial2 = AdMob.get_admob_id(2,INTERSTITIAL_KEY);
        interstitial3 = AdMob.get_admob_id(3,INTERSTITIAL_KEY);
        interstitial4 = AdMob.get_admob_id(4,INTERSTITIAL_KEY);

        Title2.setText(singleton_images.getInstance().getApp_name());

        interstitialAd.setAdUnitId(interstitial0);
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
       pd.setMessage("Please wait ..."+ "Ad Loading ...");
       pd.setIndeterminate(true);

        c=getApplicationContext();

        Intent i =getIntent();
        ads_variable=i.getBooleanExtra("ads",true);

       // timestamp=new java.sql.Timestamp(this);
        current_time=new Date().getTime();
        Log.d("current time :", String.valueOf(current_time) );



        //ringProgressDialog = ProgressDialog.show(context, "Please wait ...", "Loading ...", true);

        //ringProgressDialog.setCancelable(true);







//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);
       // Glide.with(c).load().into

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                pd.show();
                //interstitialAd.setAdUnitId(interstitial0);

                Boolean ad1_status = singleton_images.getInstance().get_ad_status(0);
                Log.e(TAG,ad1_status.toString());
                if(ad1_status==Boolean.FALSE){
                    Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                    startActivity(i);
                    pd.dismiss();


                }


                if(ad1_status==Boolean.TRUE) {
                    interstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            Toast.makeText(getApplicationContext(),"Thanks for watching add...",Toast.LENGTH_LONG).show();
                            Intent i = new Intent(Activity_01.this, Activity_SetOne.class);
                            startActivity(i);
                            pd.dismiss();


                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);

                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                        }

                        @Override
                        public void onAdOpened() {
                            super.onAdOpened();
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            //set the last loaded timestamp
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            interstitialAd.show();
                            pd.dismiss();

                        }
                    });
                    interstitialAd.loadAd(adRequest);
                }
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                //pd.setCancelable(true);
                //interstitialAd.setAdUnitId(interstitial1);

                Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                Log.e(TAG,ad1_status.toString());
                if(ad1_status==Boolean.FALSE){
                    Intent i = new Intent(Activity_01.this, Activity_SetTwo.class);
                    startActivity(i);
                    pd.dismiss();

                }
                if (ad1_status==Boolean.TRUE) {

                    interstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            Toast.makeText(getApplicationContext(),"Thanks for watching add...",Toast.LENGTH_LONG).show();

                            Intent i = new Intent(Activity_01.this, Activity_SetTwo.class);
                            startActivity(i);
                            pd.dismiss();
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                        }

                        @Override
                        public void onAdOpened() {
                            super.onAdOpened();
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            interstitialAd.show();
                            pd.dismiss();

                        }
                    });
                    interstitialAd.loadAd(adRequest);
                }
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd.show();
                //pd.setCancelable(true);
                //interstitialAd.setAdUnitId(interstitial2);

                Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                Log.e(TAG,ad1_status.toString());
                if(ad1_status==Boolean.FALSE){
                    Intent i = new Intent(Activity_01.this, Activity_SetThree.class);
                    startActivity(i);
                    pd.dismiss();
                    //pd.setCancelable(true);

                }
                if (ad1_status==Boolean.TRUE) {
                    interstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            Toast.makeText(getApplicationContext(),"Thanks for watching add...",Toast.LENGTH_LONG).show();

                            Intent i = new Intent(Activity_01.this, Activity_SetThree.class);
                            startActivity(i);
                            pd.dismiss();
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {

                            super.onAdFailedToLoad(i);
                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                        }

                        @Override
                        public void onAdOpened() {
                            super.onAdOpened();
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            Toast.makeText(getApplicationContext(),"Thanks for watching add...",Toast.LENGTH_LONG).show();

                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            interstitialAd.show();
                            pd.dismiss();

                        }
                    });
                    interstitialAd.loadAd(adRequest);
                }
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               pd.show();
                //pd.setCancelable(true);

                Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                Log.e(TAG,ad1_status.toString());
                if(ad1_status==Boolean.FALSE){
                    Intent i = new Intent(Activity_01.this, Activity_SetFour.class);
                    startActivity(i);
                    pd.dismiss();

                }
                if (ad1_status==Boolean.TRUE) {
                    interstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            Toast.makeText(getApplicationContext(),"Thanks for watching add...",Toast.LENGTH_LONG).show();

                            Intent i = new Intent(Activity_01.this, Activity_SetFour.class);
                            startActivity(i);
                            pd.dismiss();
                        }

                        @Override
                        public void onAdFailedToLoad(int i) {
                            super.onAdFailedToLoad(i);
                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                        }

                        @Override
                        public void onAdLeftApplication() {
                            super.onAdLeftApplication();
                        }

                        @Override
                        public void onAdOpened() {
                            super.onAdOpened();
                        }

                        @Override
                        public void onAdLoaded() {
                            super.onAdLoaded();
                            //set the last loaded timestamp even if the ad load fails
                            singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            interstitialAd.show();
                            pd.dismiss();

                        }
                    });
                    interstitialAd.loadAd(adRequest);
                }
            }
        });





    }


//    @Override
//    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        int id =item.getItemId();
//        if(id==nav_camera){
//            Intent i=new Intent(Activity_01.this,PrivacyPolicy.class);
//            startActivity(i);
//
//
//        }
//        else if(id==nav_gallery){
//            Intent i=new Intent(Activity_01.this,PrivacyPolicy.class);
//            startActivity(i);
//
//        }
//        else if(id==nav_manage){
//            Intent i=new Intent(Activity_01.this,PrivacyPolicy.class);
//            startActivity(i);
//
//        }
//        else if(id==nav_slideshow){
//            Intent i=new Intent(Activity_01.this,PrivacyPolicy.class);
//            startActivity(i);
//
//        }
//
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        drawer.closeDrawer(GravityCompat.START);
//
//        return true;
//    }






}
