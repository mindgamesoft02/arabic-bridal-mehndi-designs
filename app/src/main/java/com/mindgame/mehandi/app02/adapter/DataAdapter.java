package com.mindgame.mehandi.app02.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mindgame.mehandi.app02.Imageview_SetOne;
import com.mindgame.mehandi.app02.R;


import java.util.ArrayList;

/**
 * Created by lenovo on 16-Apr-17.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private ArrayList<String> loadingimags;
    private Context context;

    public DataAdapter(Context context, ArrayList<String> loading_imags) {
        this.context = context;
        this.loadingimags = loading_imags;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        String str = (String) this.loadingimags.get(i);
        setUpImage(viewHolder.img, str);

       /* Glide.with(context).load(loadingimags.get(i))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.img);*/
        // viewHolder.img.setImageURI(loadingimags.get(i));
        //     Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
        //  Picasso.with(context).load(loadingimags.get(i).getImage_url()).into(viewHolder.img);

        //Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
        //Glide.with(context).load(str).into(viewHolder.img);

    }


    @Override
    public int getItemCount() {
        return loadingimags.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView img;

        public ViewHolder(final View view) {
            super(view);

            img = (ImageView) view.findViewById(R.id.img1);


            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent localIntent = new Intent(context, Imageview_SetOne.class);
                    Bundle galleryBundle = new Bundle();
                    galleryBundle.putStringArrayList("images", loadingimags);
                    galleryBundle.putInt("POS", getPosition());
                    localIntent.putExtras(galleryBundle);
                    localIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(localIntent);
                    //context.startActivity(new Intent(v.getContext(), ImageviewActivity.class).putExtra("POS", getPosition()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                }
            });
        }

        @Override
        public void onClick(View v) {

        }
    }

    private void setUpImage(ImageView paramImageView, String paramString) {
        if (!TextUtils.isEmpty(paramString)) {


            Glide.with(paramImageView.getContext()).load(paramString)
                    .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(paramImageView);
            //Picasso.with(paramImageView.getContext()).load(paramString).resize(this.mGridItemWidth,this.mGridItemHeight).centerCrop().into(paramImageView);
            return;
        }
        paramImageView.setImageDrawable(null);
    }
}