package com.mindgame.mehandi.app02;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;
import com.mindgame.mehandi.app02.adapter.CustomPagerAdapter;
import com.mindgame.mehandi.app02.indicator.CirclePageIndicator;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Imageview_SetTwo extends Activity {

    CustomPagerAdapter customPagerAdapter;
    CirclePageIndicator indicator;
    ViewPager viewPager;
    TextView indicateimages;
    int val;
    private List<String> mImages;
    private ArrayList<String> localarraaylist_fullscreen;
    int position;
    AdMobdetails_singleton ad = AdMobdetails_singleton.getInstance() ;
    LinearLayout layout, strip, layout1, strip1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imageview);
        localarraaylist_fullscreen = new ArrayList<>();

        indicateimages = (TextView) findViewById(R.id.indicateimages);
        //final ArrayList image_Urls = prepareData();
        Bundle fb = getIntent().getExtras();
        this.mImages = fb.getStringArrayList("images");
        this.position = fb.getInt("POS");
        localarraaylist_fullscreen.addAll(this.mImages);


        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner(getApplicationContext());

        customPagerAdapter = new CustomPagerAdapter(this, localarraaylist_fullscreen);
        viewPager = (ViewPager) findViewById(R.id.viewpager_suit);
        viewPager.setClipToPadding(false);
        viewPager.setPageMargin(25);

        viewPager.setPadding(45, 0, 45, 0);

        viewPager.setAdapter(customPagerAdapter);




        viewPager.setCurrentItem(position);
        indicateimages.setText(viewPager.getCurrentItem() + 1 + "/" + localarraaylist_fullscreen.size());

        findViewById(R.id.saveImagebutton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                svaeImage();
                val = 0;
            }

        });
        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                svaeImage();
                val = 1;
            }

        });
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            // optional
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            // optional
            @Override
            public void onPageSelected(int position) {
            }

            // optional
            @Override
            public void onPageScrollStateChanged(int state) {
                indicateimages.setText(viewPager.getCurrentItem() + 1 + "/" + localarraaylist_fullscreen.size());

            }
        });

        findViewById(R.id.myimags).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MyImages.class));
            }
        });

        findViewById(R.id.rate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("https://play.google.com/store/apps/details?id=" + getApplicationContext().getPackageName())));


            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    private void svaeImage() {

        if (AppStatus.getInstance(Imageview_SetTwo.this).isOnline()) {
            new AsyncTaskLoadImage().execute(localarraaylist_fullscreen.get(viewPager.getCurrentItem()));

        } else {
            Toast.makeText(getApplicationContext(), "Please Check NetWork Connection", Toast.LENGTH_LONG).show();
        }
    }




    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Imageview Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
    }


    public class AsyncTaskLoadImage extends AsyncTask<String, String, Bitmap> {
        private final static String TAG = "AsyncTaskLoadImage";
        ProgressDialog ringProgressDialog;

        public AsyncTaskLoadImage() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ringProgressDialog = ProgressDialog.show(Imageview_SetTwo.this, "Please wait ...", "Downloading Image ...", true);

            ringProgressDialog.setCancelable(true);

        }

        @Override
        protected Bitmap doInBackground(String... params) {
            Bitmap bitmap = null;
            try {
                URL url = new URL(params[0]);
                bitmap = BitmapFactory.decodeStream((InputStream) url.getContent());


            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {


            ringProgressDialog.dismiss();


            shareandSave(bitmap);

        }
    }

    private void shareandSave(Bitmap bmp) {
        if (val == 1) {
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("image/*");

            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.jpg");
            try {
                f.createNewFile();
                FileOutputStream fo = new FileOutputStream(f);
                fo.write(bytes.toByteArray());
            } catch (IOException e) {
                e.printStackTrace();
            }
            share.putExtra(Intent.EXTRA_STREAM, Uri.parse("file:///sdcard/temp.jpg"));
            try {
                startActivity(Intent.createChooser(share, "Share via"));
            } catch (ActivityNotFoundException ex) {
                Toast.makeText(getApplicationContext(), "Please Connect To Internet", Toast.LENGTH_LONG)
                        .show();
            }

        } else {
            saveImageToExternalStorage(bmp);
        }
    }

    void saveImageToExternalStorage(Bitmap b) {

        Calendar c = Calendar.getInstance();
        SimpleDateFormat sf = new SimpleDateFormat("yyyyMMdd'T'HHmmss");
        String timeStamp = sf.format(new Date());

        String fullPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ViewPagerSavedImages/";

        String temp_path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;


        try {
            File dir_temp = new File(temp_path);
            if (!dir_temp.exists()) {
                dir_temp.mkdirs();
            }
            OutputStream fOut_temp = null;
            File file_temp = new File(temp_path, "ViewPagerSavedImages" + ".jpg");
            if (file_temp.exists())
                file_temp.delete();
            file_temp.createNewFile();
            fOut_temp = new FileOutputStream(file_temp);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut_temp);
            fOut_temp.flush();
            fOut_temp.close();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

        try {
            File dir = new File(fullPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            OutputStream fOut = null;
            File file = new File(fullPath, "picture" + timeStamp + ".jpg");
            if (file.exists())
                file.delete();
            file.createNewFile();
            fOut = new FileOutputStream(file);
            // 100 means no compression, the lower you go, the stronger the
            // compression
            b.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            Log.e("saveToExternalStorage()", e.getMessage());
        }

    }
    /** Called when leaving the activity */
    @Override
    public void onPause() {
        if ( ad.adView!= null) {
            ad.adView.pause();
        }
        super.onPause();
    }

    /** Called when returning to the activity */
    @Override
    public void onResume() {
        super.onResume();
        //app_name.setText(singleton_images.getInstance().getApp_name());
        if (ad.adView!= null) {
            ad.adView.resume();
        }
    }

    /** Called before the activity is destroyed */
    @Override
    public void onDestroy() {

        if (ad.adView!= null) {
            ad.adView.destroy();
        }
        super.onDestroy();
    }

}
